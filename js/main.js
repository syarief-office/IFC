$(function(){
	
	var Obj = {
		initialize: function(){
			new WOW().init();
            $('select').selectpicker();
            $( ".datepicker" ).datepicker();
            $('.square').boxSquare();
            $('.left-topcarousel').on('click',function(){
                $('#top-carousel').carousel('prev');
            });
            $('.right-topcarousel').on('click',function(){
                $('#top-carousel').carousel('next');
            });
            $('.left-gallery3').on('click',function(){
                $('#gallery3').carousel('prev');
            });
            $('.right-gallery3').on('click',function(){
                $('#gallery3').carousel('next');
            });
            $('#tab-content a').on('click',function(e){
                e.preventDefault();
                $(this).tab('show');
            });
            $('.expander').each(function(){
                $(this).on('click',function(e){
                   var dt = $(this).attr('data-target');
                    //$('.expander').removeClass('expander-active');
                    if( $(dt).is(':hidden') ){
                        $(dt).slideDown();
                         $(this).children('span').children('i.fa-angle-down').removeClass('fa-angle-down').addClass('fa-angle-up');
                         $(this).addClass('expander-active');
                    } else {
                        $(dt).slideUp();
                        $(this).children('span').children('i.fa-angle-up').removeClass('fa-angle-up').addClass('fa-angle-down');
                        $('.expander').removeClass('expander-active');
                    }
                });
            });
            $('.toggle-view').each(function(){
                $(this).on('click',function(e){
                    var dv = $(this).attr('data-view');
                    $('.toggle-view').removeClass('active-view');
                    $(this).addClass('active-view');
                    if( dv == 'block' ){
                        $('#view-list').children('div').children('div').removeClass('col-sm-4').addClass('col-sm-12');
                    } else {
                        $('#view-list').children('div').children('div').removeClass('col-sm-12').addClass('col-sm-4');
                    }
                });
            });
		},
        form: function(){
            $('.click-upload').on('click',function(e){
                e.preventDefault();
                $(this).next('.upload_file').trigger('click');
            });
            function clickadd(el, target, ins){
                $(el).on('click',function(e, i){
                    //var i = 1, j = i++ + 1;
                    var html = $('.'+target+'').clone(true, true);
                    //var upload_inp = html.find('.upload_file');
                    //upload_inp.attr('name','portofolio'+ j++);
                    html.removeClass(target);
                    html.removeClass('xhide');
                    html.css('display','block');
                    html.insertBefore(ins);
                });
            }
            clickadd('.porto-upload','multi-upload-clone','.multi-upload-line');
            clickadd('.media-news-upload','multi-upload-clone2','.multi-upload-line2');
        },
		run: function(){
            $('a.smoothScroll[href*="#"]:not([href="#"])').click(function() {
                if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
                  var target = $(this.hash);
                  target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
                  if (target.length) {
                    $('html, body').animate({
                      scrollTop: target.offset().top
                    }, 1000);
                    return false;
                  }
                }
             });

            $(window).scroll(function() {
            if ($(this).scrollTop() > 1){  
                $('.header-wrap').addClass("stickyHead");
              }
              else{
                $('.header-wrap').removeClass("stickyHead");
              }
            });
		}
	};

    $(window).on('load',function(){
	    Obj.run();
        Obj.form();
        Obj.initialize();
    });

});
